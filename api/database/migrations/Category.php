<?php

namespace database\migrations;


use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Blueprint;

class Category
{
    public function up()
    {
        if (Manager::schema()->hasTable('categories')) {
            return;
        }
        Manager::schema()->create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }
}