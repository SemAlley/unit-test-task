<?php

namespace database\migrations;


use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Blueprint;

class ProductCategory
{
    public function up()
    {
        if (Manager::schema()->hasTable('product_category')) {
            return;
        }
        Manager::schema()->create('product_category', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')
                  ->on('categories')->onDelete('cascade');

            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')
                  ->on('products')->onDelete('cascade');

            $table->timestamps();
        });
    }
}