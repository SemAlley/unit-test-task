<?php

namespace database\migrations;


use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Blueprint;

class Product
{
    public function up()
    {
        if (Manager::schema()->hasTable('products')) {
            return;
        }
        Manager::schema()->create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('price');
            $table->timestamps();
        });
    }
}