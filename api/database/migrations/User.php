<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 17.04.18
 * Time: 22:55
 */

namespace database\migrations;


use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Blueprint;

class User
{
    public function up()
    {
        if (Manager::schema()->hasTable('users')) {
            return;
        }
        Manager::schema()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamps();
        });
    }
}