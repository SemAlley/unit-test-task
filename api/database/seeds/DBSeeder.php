<?php

namespace database\seeds;


class DBSeeder
{
    protected function registeredSeeders(): array {
        return [
            'database\seeds\CategoriesSeeder',
            'database\seeds\ProductsSeeder'
        ];
    }

    public function run(): void {
        $seeders = $this->registeredSeeders();
        foreach ($seeders as $seeder) {
            (new $seeder)->run();
        }
    }
}