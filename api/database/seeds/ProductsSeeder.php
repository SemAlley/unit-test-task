<?php

namespace database\seeds;


use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Capsule\Manager;

class ProductsSeeder
{
    public function run(): void {
        Manager::table('products')->delete();
        $categories = Category::all();
        foreach ($categories as $category) {
            for ($i = 0; $i < 10; $i++) {
                $product = new Product([
                    'name' => 'product_' . $i,
                    'price' => mt_rand(1000, 10000) / 100
                ]);
                $category->products()->save($product);
            }
        }
    }
}