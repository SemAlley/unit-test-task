<?php

namespace database\seeds;


use Illuminate\Database\Capsule\Manager;

class CategoriesSeeder
{
    public function run(): void {
        Manager::table('categories')->delete();
        Manager::table('categories')->insert([
            [
                'name' => 'Sport',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'name' => 'Fashion',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'name' => 'Classic',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);
    }
}