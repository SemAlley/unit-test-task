<?php

require __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/app/helpers.php';

$app = new \Bootstrap\App(realpath(__DIR__ . '/'));

(new \Core\Init)->run();