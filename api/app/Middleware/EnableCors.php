<?php

namespace App\Middleware;

use Core\Contracts\IHandleRequest;
use Symfony\Component\HttpFoundation\Response;

class EnableCors implements IHandleRequest
{
    /**
     * @return bool
     */
    public function handleRequest(): bool
    {
        $response = app('response');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization');
        $response->headers->set('Access-Control-Max-Age', 3600);
        return true;
    }
}