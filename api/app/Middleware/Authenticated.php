<?php

namespace App\Middleware;

use Core\Contracts\IHandleRequest;
use Symfony\Component\HttpFoundation\Response;

class Authenticated implements IHandleRequest
{
    /**
     * @return bool
     */
    public function handleRequest(): bool
    {
        if (app('auth')->logged()) {
            return true;
        } else {
            /**
             * @var Response $response
             */
            $response = app('response');
            $response->setContent(json_encode(['success' => false, 'message' => 'Not Authenticated']));
            $response->setStatusCode(401);
            $response->send();

            return false;
        }
    }
}