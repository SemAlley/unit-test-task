<?php

namespace App\Controllers;


use App\Models\Category;
use App\Models\Product;
use Core\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('cors');
        $this->middleware('auth', ['list']);
    }

    /**
     * @param null $category_id
     *
     * @return string
     */
    public function list($category_id = null): string
    {
        if ( ! $category_id || ! ($category = Category::find($category_id))) {
            return json_encode(['success' => false, 'message' => 'No such category']);
        }

        return $this->response()->setContent($category->products()->get()->toJson());
    }

    /**
     * @return Response
     */
    public function create(): Response
    {

        $request    = $this->request();
        $response   = $this->response();
        $name       = $request->get('name');
        $price      = $request->get('price');
        $categoryId = $request->get('category_id');
        if ( ! $name || ! $price) {
            $response->setContent(json_encode([
                'success' => false,
                'error'   => ['required' => 'Name and price required']
            ]));

            return $response;
        }
        $product = new Product(['name' => $name, 'price' => $price]);

        if ($categoryId && ($category = Category::find($categoryId))) {
            $category->products()->save($product);
        } else {
            $product->save();
        }
        $response->setContent(json_encode([
            'success' => true,
            'product' => $product
        ]));

        return $response;
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function update($id): Response
    {
        $request  = $this->request();
        $response = $this->response();
        $name     = $request->get('name');
        $price    = $request->get('price');

        $product = Product::find($id);

        if ( ! $product) {
            $response->setContent(json_encode([
                'success' => false,
                'error'   => ['product' => 'Wrong product id']
            ]));

            return $response;
        }
        if ($name) {
            $product->name = $name;
        }
        if ($price) {
            $product->price = $price;
        }

        $product->save();
        $response->setContent(json_encode([
            'success' => true,
            'product' => $product
        ]));

        return $response;
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function delete($id): Response
    {
        $response = $this->response();

        $product = Product::find($id);

        if ( ! $product) {
            $response->setContent(json_encode([
                'success' => false,
                'error'   => ['product' => 'Wrong product id']
            ]));

            return $response;
        }
        $product->delete();
        $response->setContent(json_encode([
            'success' => true
        ]));

        return $response;
    }
}