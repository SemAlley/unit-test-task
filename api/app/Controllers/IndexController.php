<?php

namespace App\Controllers;


use Core\Controller;

class IndexController extends Controller
{

    /**
     * @return string
     */
    public function index()
    {
        return json_encode(['status' => 'success']);
    }
}