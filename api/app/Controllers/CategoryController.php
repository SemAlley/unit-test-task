<?php

namespace App\Controllers;


use App\Models\Category;
use Core\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('cors');
        $this->middleware('auth', ['list']);
    }

    /**
     * @return Response
     */
    public function list(): Response
    {
        return $this->response()->setContent(Category::all()->toJson());
    }

    /**
     * @return Response
     */
    public function create(): Response
    {
        $request  = $this->request();
        $response = $this->response();
        $name     = $request->get('name');
        if ( ! $name) {
            $response->setContent(json_encode([
                'success' => false,
                'error'   => ['required' => 'Name is required']
            ]));

            return $response;
        }

        $category = new Category(['name' => $name]);
        $category->save();

        $response->setContent(json_encode([
            'success'  => true,
            'category' => $category
        ]));

        return $response;
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function update($id): Response
    {
        $request  = $this->request();
        $response = $this->response();
        $name     = $request->get('name');

        $category = Category::find($id);

        if ( ! $category) {
            $response->setContent(json_encode([
                'success' => false,
                'error'   => ['product' => 'Wrong category id']
            ]));

            return $response;
        }
        if ($name) {
            $category->name = $name;
        }

        $category->save();
        $response->setContent(json_encode([
            'success'  => true,
            'category' => $category
        ]));

        return $response;
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function delete($id): Response
    {
        $response = $this->response();

        $category = Category::find($id);

        if ( ! $category) {
            $response->setContent(json_encode([
                'success' => false,
                'error'   => ['category' => 'Wrong category id']
            ]));

            return $response;
        }
        $category->delete();
        $response->setContent(json_encode([
            'success' => true
        ]));

        return $response;
    }
}