<?php

namespace App\Controllers;


use App\Models\User;
use Core\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('cors');
    }

    /**
     * @return Response
     */
    public function register(): Response
    {
        /**
         * @var Request $request
         */
        $request  = $this->request();
        $email    = $request->get('email');
        $password = $request->get('password');

        $errors   = $this->validateCreds($request);
        $response = $this->response();
        if ( ! empty($errors)) {
            $response->setContent(json_encode(['success' => false, 'errors' => $errors]));

            return $response;
        }
        if (User::where('email', $email)->first()) {
            $response->setContent(json_encode(['success' => false, 'errors' => ['email' => 'User already exists']]));

            return $response;
        }
        $user           = new User();
        $user->email    = $email;
        $user->password = password_hash($password, PASSWORD_BCRYPT);
        $user->save();

        $token = app('auth')->login($user);

        $response->setContent(json_encode(['success' => true, 'token' => $token]));

        return $response;
    }

    /**
     * @return Response
     */
    public function login(): Response
    {
        /**
         * @var Request $request
         */
        $request  = $this->request();
        $email    = $request->get('email');
        $password = $request->get('password');

        $errors = $this->validateCreds($request);

        $response = $this->response();
        if ( ! empty($errors)) {
            $response->setContent(json_encode(['success' => false, 'errors' => $errors]));

            return $response;
        }

        $user = User::where('email', $email)->first();

        if ( ! password_verify($password, $user->password)) {
            $response->setContent(json_encode([
                'success' => false,
                'errors'  => ['password' => 'Wrong password']
            ]));

            return $response;
        }

        if ( ! $user) {
            $response->setContent(json_encode([
                'success' => false,
                'errors'  => ['credentials' => 'Wrong credentials']
            ]));

            return $response;
        }

        $token = app('auth')->login($user);

        $response->setContent(json_encode(['success' => true, 'token' => $token]));

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    protected function validateCreds(Request $request): array
    {
        $email    = $request->get('email');
        $password = $request->get('password');
        $errors   = [];
        if ( ! $email && ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'Not valid email';
        }
        if ( ! $password) {
            $errors['password'] = 'Password is required';
        }

        return $errors;
    }
}