<?php

if ( ! function_exists('app')) {
    /**
     * Get the available container instance.
     *
     * @param  string  $make
     * @return mixed|\Bootstrap\App
     */
    function app($make = null)
    {
        if (is_null($make)) {
            return \Bootstrap\App::getInstance();
        }

        return \Bootstrap\App::getInstance()->make($make);
    }
}

if( ! function_exists('base_path')) {
    function base_path() {
        return app()->getBasePath();
    }
}