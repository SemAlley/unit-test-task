<?php

namespace app\Models;


use Core\Contracts\Authentificable;
use Core\Model;

class User extends Model implements Authentificable
{
    protected $table = 'users';

    public function getAuthData(): array
    {
        return [
            'id'    => $this->id,
            'email' => $this->email
        ];
    }
}