<?php

namespace app\Models;


use Core\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name'];

    public function products() {
        return $this->belongsToMany('App\Models\Product', 'product_category')->withTimestamps();
    }
}