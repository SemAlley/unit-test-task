<?php

namespace app\Models;


use Core\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'price'];

    public function category() {
        return $this->belongsToMany('App\Models\Category', 'product_category')->withTimestamps();
    }
}