<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 16.04.18
 * Time: 22:41
 */

namespace Bootstrap;


use Core\Auth;
use Core\Config;
use Core\Contracts\IHandleRequest;
use Core\Middleware;
use Core\Router;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerContract;
use Illuminate\Database\Capsule\Manager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Parser;

class App extends Container
{

    /**
     * @var Router
     */
    protected $router;
    /**
     * @var string
     */
    protected $basePath;

    /**
     * App constructor.
     *
     * @param string $basePath
     */
    public function __construct(string $basePath)
    {
        try {
            $this->basePath = $basePath;
            $this->bootstrapContainer($this);
            $this->bootstrapDatabase();
            $this->bootstrapRouter();
            $this->initAuth();
        } catch (\Exception $e) {
            $response = new Response(json_encode(['success' => false, 'error' => ['exception' => $e->getMessage()]]),
                503);
            $response->send();
            die();
        }
    }

    /**
     * @param ContainerContract $container
     */
    protected function bootstrapContainer(ContainerContract $container): void
    {
        self::setInstance($container);

        $this->instance('app', $this);
        $this->instance('request', Request::createFromGlobals());
        $this->instance('response', new Response());
        $this->instance('file_locator', new FileLocator(array($this->getBasePath() . '/config')));
        $this->instance('route_loader', new YamlFileLoader($this->make('file_locator')));
        $this->instance('parser', new Parser());
        $this->instance('conf', new Config());
        $this->instance('middleware', new Middleware());
        $this->instance('auth', new Auth($this->make('conf')->get('jwt')));
    }

    /**
     * Init router
     */
    protected function bootstrapRouter(): void
    {
        $this->router = new Router($this->basePath);
    }

    /**
     * Run application
     */
    public function run(): void
    {
        $this->router->run();
    }

    /**
     * Init database
     */
    protected function bootstrapDatabase(): void
    {
        $capsule = new Manager($this);

        $capsule->addConnection($this->make('conf')->get('db'));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

    /**
     * Init Auth
     */
    protected function initAuth()
    {
        $this->make('auth')->init($this->make('request'));
    }

    /**
     * @param string $name
     * @param IHandleRequest $middleware
     */
    public function withMiddleware(string $name, IHandleRequest $middleware)
    {
        $this->make('middleware')->add($name, $middleware);
    }

    /**
     * @return string
     */
    public function getBasePath(): string
    {
        return $this->basePath;
    }
}