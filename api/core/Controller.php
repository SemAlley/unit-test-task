<?php

namespace Core;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class Controller
{

    /**
     * @var array
     */
    protected $middlewares = [];

    /**
     * Redirect to route
     *
     * @param string $route
     * @param array $params
     */
    protected function redirect(string $route, array $params = []): void
    {
        $url = app()->getUrlGenerator()->generate($route, $params);
        header('Location: ' . $url);
    }

    /**
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function forceAction($method, $args)
    {
        if ( ! method_exists($this, $method)) {
            $this->redirect('index');
        }
        $stop = false;
        foreach ($this->middlewares as $middleware => $excepts) {
            if (in_array($method, $excepts)) {
                continue;
            }
            if ( ! app('middleware')->handle($middleware)) {
                $stop = true;
            }
        }
        if ($stop) {
            return '';
        }

        return $this->{$method}(...$args);
    }

    /**
     * @param string $name
     * @param array $except
     */
    protected function middleware(string $name, array $except = [])
    {
        $this->middlewares[$name] = $except;
    }

    /**
     * @return Response
     */
    protected function response(): Response
    {
        return app('response');
    }

    /**
     * @return Request
     */
    protected function request(): Request
    {
        return app('request');
    }
}