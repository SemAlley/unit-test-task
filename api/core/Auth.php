<?php

namespace Core;


use Core\Contracts\Authentificable;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;

class Auth
{
    /**
     * @var mixed|string
     */
    protected $secret;
    /**
     * @var mixed|null
     */
    protected $authModel;
    /**
     * @var int
     */
    protected $ttl;
    /**
     * @var mixed|string
     */
    protected $serverName;
    /**
     * @var Authentificable|null
     */
    protected $user = null;

    /**
     * Auth constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->secret     = $config['secret'] ?? 'secret';
        $this->authModel  = $config['auth_model'] ?? null;
        $this->ttl        = (int)$config['ttl'] ?? 360;
        $this->serverName = $config['server_name'] ?? '';
    }

    /**
     * @param Authentificable $user
     *
     * @return string
     */
    public function login(Authentificable $user): string
    {
        $tokenId    = base64_encode(random_bytes(32));
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;
        $expire     = $notBefore + ($this->ttl * 60);
        $serverName = $this->serverName;
        $data       = [
            'iat'  => $issuedAt,
            'jti'  => $tokenId,
            'iss'  => $serverName,
            'nbf'  => $notBefore,
            'exp'  => $expire,
            'data' => $user->getAuthData()
        ];
        $jwt        = JWT::encode(
            $data,
            $this->secret,
            'HS512'
        );

        return $jwt;
    }

    /**
     * @return Authentificable
     */
    public function user(): Authentificable
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function logged(): bool
    {
        return !is_null($this->user);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function init(Request $request)
    {
        $authHeader = $request->headers->get('authorization');
        if ($authHeader) {
            list($jwt) = sscanf($authHeader, 'Bearer %s');
            try {
                $token = JWT::decode($jwt, $this->secret, array('HS512'));
                if (isset($token->data) && class_exists($this->authModel)) {
                    $user = (new $this->authModel)->find($token->data->id);
                    if ($user) {
                        $this->user = $user;
                    }
                }
            } catch (ExpiredException $e) {
                return false;
            }
        }

        return true;
    }
}