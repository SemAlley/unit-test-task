<?php

namespace Core;


use Core\Contracts\IHandleRequest;

class Middleware
{
    /**
     * @var array
     */
    protected $middlewares = [];

    /**
     * @param string $name
     * @param IHandleRequest $instance
     */
    public function add(string $name, IHandleRequest $instance)
    {
        $this->middlewares[$name] = $instance;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function handle(string $name)
    {
        if ( ! array_key_exists($name, $this->middlewares)) {
            return true;
        }
        return $this->middlewares[$name]->handleRequest();
    }
}