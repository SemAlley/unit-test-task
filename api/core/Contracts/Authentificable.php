<?php

namespace Core\Contracts;


interface Authentificable
{
    function getAuthData(): array;
}