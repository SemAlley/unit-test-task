<?php

namespace Core\Contracts;


interface IHandleRequest
{
    function handleRequest(): bool;
}