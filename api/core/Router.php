<?php

namespace Core;


use ReflectionMethod;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouteCollection;

class Router
{
    /**
     * @var string $basePath
     */
    protected $basePath;

    /**
     * @var FileLocator $fileLocator
     */
    protected $fileLocator;

    /**
     * @var YamlFileLoader $loader
     */
    protected $loader;

    /**
     * @var RouteCollection $routes
     */
    protected $routes;

    /**
     * @var RequestContext $context
     */
    protected $context;

    /**
     * @var UrlGenerator $urlGenerator
     */
    protected $urlGenerator;

    protected $currentRoute;

    /**
     * Router constructor.
     *
     * @param $basePath
     */
    public function __construct($basePath)
    {
        $this->basePath = $basePath;
        $this->initDependencies();
        $this->initRoutes();
    }

    /**
     * Init Router dependencies
     *
     * @return void
     */
    protected function initDependencies(): void
    {
        $this->fileLocator = app('file_locator');
        $this->loader      = app('route_loader');
        $this->context     = new RequestContext();
    }

    /**
     * Init routes
     *
     * @return void
     */
    protected function initRoutes(): void
    {
        $request = Request::createFromGlobals();
        $routes  = $this->loader->load('routes.yaml');
        $this->context->fromRequest($request);

        $matcher = new UrlMatcher($routes, $this->context);

        $this->urlGenerator = new UrlGenerator($routes, $this->context);

        $this->currentRoute = $matcher->matchRequest($request);

    }

    /**
     * Proceed route
     */
    public function run(): void
    {
        $controllerParts  = explode('@', $this->currentRoute['_controller']);
        $controllerClass  = '\\App\\Controllers\\' . array_shift($controllerParts);
        $controllerMethod = array_shift($controllerParts);
        if (class_exists($controllerClass) && method_exists($controllerClass, $controllerMethod)) {
            $r            = new ReflectionMethod($controllerClass, $controllerMethod);
            $params       = [];
            $methodParams = $r->getParameters();
            foreach ($methodParams as $param) {
                if (isset($param->name, $this->currentRoute) && $this->currentRoute[$param->name]) {
                    $params[] = $this->currentRoute[$param->name];
                }
            }
            $response = (new $controllerClass)->forceAction($controllerMethod, $params);
            if ( ! ($response instanceof Response)) {
                $response = new Response($response);
            }

            $response->headers->set('Content-Type', 'application/json');
            $response->send();
        } else {
            echo '404';
        }
    }

    /**
     * Get UrlGenerator instance
     *
     * @return UrlGenerator
     */
    public function getUrlGenerator(): UrlGenerator
    {
        return $this->urlGenerator;
    }
}