<?php

namespace Core;


use database\seeds\DBSeeder;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

class Init
{
    /**
     * Run migrations and seeders
     */
    public function run()
    {
        $this->runMigrations();
        $this->runSeeders();
    }

    /**
     * Run migrations
     */
    protected function runMigrations() {
        $path = app()->getBasePath();
        $allFiles = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path . '/database/migrations'));
        $phpFiles = new RegexIterator($allFiles, '/\.php$/');
        foreach ($phpFiles as $file) {
            require $file->getPath() . '/' . $file->getFilename();
            $className = 'database\migrations\\' . str_replace('.php', '', $file->getFilename());
            if(class_exists($className)) {
                (new $className)->up();
            }
        }
    }

    /**
     * Run seeders
     */
    protected function runSeeders() {
        (new DBSeeder())->run();
    }
}