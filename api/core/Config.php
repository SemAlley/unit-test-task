<?php

namespace Core;


use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

class Config
{
    /**
     * Imitate caching configs
     *
     * @var array
     */
    protected $cached = [];

    /**
     * Load config by filename
     *
     * @param $name
     *
     * @return mixed|null
     */
    public function get($name)
    {
        if (array_key_exists($name, $this->cached)) {
            return $this->cached[$name];
        }
        $fileName = app()->getBasePath() . '/config/' . $name . '.yaml';
        if ( ! file_exists($fileName)) {
            return null;
        }
        try {
            $parsed              = (new Parser())->parseFile($fileName);
            $this->cached[$name] = $parsed;
            return $parsed;
        } catch (ParseException $e) {
            return null;
        }
    }

}