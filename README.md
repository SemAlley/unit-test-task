# UNIT Test task

## Install

### If use included docker:

* Run 
```
docker-compose build
docker-compose up
```

### If custom

* Set up web server to **/api/public** folder

Change api/config/db.yaml configuration


## API

### Setup

Install composer dependencies
```
cd project_path/api
composer install
```

Run migrations and seeders
```
cd project_path
docker-compose exec api php init.php
```

### Routes
_If from docker, basePath = "localhost:8888"_

 * GET|POST http://{basePath}/user/login {email: 'email@email.com', password: 'pass'}
 
  _success => {"success":true,"token":"token"}_
  
 * POST http://{basePath}/user/register {email: 'email@email.com', password: 'pass'}
 
  _success => {"success":true,"token":"token"}_
  
 * GET http://{basePath}/category/all
 
  _success => [{category}, {category}, ...]
  
 * POST http://{basePath}/category {name: "cat name"}
 
  _success => {"success":true,"category":{category}}_
  
 * PUT http://{basePath}/category/{id} {name: "cat name"}
 
  _success => {"success":true,"category":{category}}_
  
 * DELETE http://{basePath}/category/{id}
 
  _success => {"success":true}_

 * GET http://{basePath}/product/{category_id}
 
  _success => [{product}, {product}, ...]
  
 * POST http://{basePath}/product {name: "product name", price: 0.00, category_id: optional}
 
  _success => {"success":true,"product":{product}}_
  
 * PUT http://{basePath}/product/{id} {name: "product name", price: 0.00}
 
  _success => {"success":true,"product":{product}}_
  
 * DELETE http://{basePath}/product/{id}
 
  _success => {"success":true}_


Error format:
_{"success":false, error: {error_name: "Error message", another_error: "Message"}}_

### Auth

Add "_Authorization: Bearer token_" header, to use protected routes